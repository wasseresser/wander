#include "ZWApi.h"
#include "Defines.h"
#include "WARoutines.h"

#pragma warning( disable: 4706 )

extern "C" NTSTATUS WAGetProcessId( wchar_t* ProcessName_, SYSTEM_PROCESS_INFORMATION* OutputBuffer_ ) // TODO: create proper buffers
{
	ULONG SizeToAllocate{ 0 };

	auto StatusCode = ZwQuerySystemInformation( SystemProcessInformation, nullptr, 0, &SizeToAllocate );

	if ( StatusCode != STATUS_INFO_LENGTH_MISMATCH )
	{
		DPRINT( "WAGetProcessId: ZwQuerySystemInformation failed with status code: %lX\n", StatusCode );
		return StatusCode;
	}

	void* ProcessListBuffer{ nullptr };

	if ( !( ProcessListBuffer = ExAllocatePoolWithTag( PagedPool, SizeToAllocate, WANDER_POOL_TAG ) ) )
	{
		DPRINT( "WAGetProcessId: ExAllocatePoolWithTag failed\n" );
		return STATUS_INSUFFICIENT_RESOURCES;
	}

	if ( !NT_SUCCESS( StatusCode = ZwQuerySystemInformation( SystemProcessInformation, ProcessListBuffer, SizeToAllocate, &SizeToAllocate ) ) )
	{
		if ( StatusCode == STATUS_INFO_LENGTH_MISMATCH )
		{
			ExFreePoolWithTag( ProcessListBuffer, WANDER_POOL_TAG );
			return WAGetProcessId( ProcessName_, OutputBuffer_ );
		}

		DPRINT( "WAGetProcessId: ZwQuerySystemInformation failed with status code: %lX\n", StatusCode );
		ExFreePoolWithTag( ProcessListBuffer, WANDER_POOL_TAG );
		return StatusCode;
	}

	SYSTEM_PROCESS_INFORMATION* ProcessInformation = reinterpret_cast< SYSTEM_PROCESS_INFORMATION* >( ProcessListBuffer );
	
	UNICODE_STRING ProcessName{ 0 };
	RtlInitUnicodeString( &ProcessName, ProcessName_ );

	do
	{
		if ( RtlEqualUnicodeString( &ProcessInformation->ImageName, &ProcessName, TRUE ) )
		{
			*OutputBuffer_ = *ProcessInformation;

			ExFreePoolWithTag( ProcessListBuffer, WANDER_POOL_TAG );
			return StatusCode;
		}

		if ( ProcessInformation->NextEntryOffset == 0 ) break;

		ProcessInformation = reinterpret_cast< SYSTEM_PROCESS_INFORMATION* >( reinterpret_cast< char* >( ProcessInformation ) + ProcessInformation->NextEntryOffset );
	}
	while ( true );

	DPRINT( "WAGetProcessId: Process couldn't be found\n" );
	ExFreePoolWithTag( ProcessListBuffer, WANDER_POOL_TAG );
	return STATUS_UNSUCCESSFUL;
}
