#include <ntifs.h>
#include "WAnder.h"
#include "Defines.h"
#include "WARoutines.h"

extern "C" NTSTATUS WADispatchCreate( DEVICE_OBJECT* DeviceObject_, IRP* IRP_ )
{
	UNREFERENCED_PARAMETER( DeviceObject_ );

	IRP_->IoStatus.Status = STATUS_SUCCESS;
	IRP_->IoStatus.Information = 0;

	IoCompleteRequest( IRP_, IO_NO_INCREMENT );

	return STATUS_SUCCESS;
}

extern "C" NTSTATUS WADispatchClose( DEVICE_OBJECT* DeviceObject_, IRP* IRP_ )
{
	UNREFERENCED_PARAMETER( DeviceObject_ );

	IRP_->IoStatus.Status = STATUS_SUCCESS;
	IRP_->IoStatus.Information = 0;

	IoCompleteRequest( IRP_, IO_NO_INCREMENT );

	return STATUS_SUCCESS;
}

extern "C" NTSTATUS WADispatchDeviceControl( DEVICE_OBJECT* DeviceObject_, IRP* IRP_ )
{
	UNREFERENCED_PARAMETER( DeviceObject_ );

	IO_STACK_LOCATION* IRPStack = IoGetCurrentIrpStackLocation( IRP_ );

	if ( !IRPStack->Parameters.DeviceIoControl.Type3InputBuffer || !IRPStack->Parameters.DeviceIoControl.InputBufferLength )
	{
		IRP_->IoStatus.Status = STATUS_INVALID_BUFFER_SIZE;
		IoCompleteRequest( IRP_, IO_NO_INCREMENT );
		IRP_->IoStatus.Information = 0;
		return STATUS_INVALID_BUFFER_SIZE;
	}

	switch ( IRPStack->Parameters.DeviceIoControl.IoControlCode )
	{
		case IOCTL_WANDER_GET_PROCESS_ID:
			WAGetProcessId( reinterpret_cast< wchar_t* >( IRPStack->Parameters.DeviceIoControl.Type3InputBuffer ), reinterpret_cast< SYSTEM_PROCESS_INFORMATION* >( IRP_->UserBuffer ) );
			break;
		default: break;
	}

	IRP_->IoStatus.Status = STATUS_SUCCESS;
	IRP_->IoStatus.Information = 0;
	IoCompleteRequest( IRP_, IO_NO_INCREMENT );
	return STATUS_SUCCESS;
}

extern "C" VOID WAUnload( DRIVER_OBJECT* DriverObject_ )
{
	UNREFERENCED_PARAMETER( DriverObject_ );

	IoDeleteDevice( WADeviceObject );
	auto StatusCode = IoDeleteSymbolicLink( &WASymbolicLinkName );

	if ( NT_SUCCESS( StatusCode ) )
		DPRINT( "WAUnload: Driver unloaded \n" );
	else
		DPRINT( "WAUnload: IoDeleteSymbolicLink failed with status code %lX \n", StatusCode );
}

extern "C" NTSTATUS DriverEntry( DRIVER_OBJECT* DriverObject_, UNICODE_STRING* RegistryPath_ )
{
	UNREFERENCED_PARAMETER( RegistryPath_ );

	UNICODE_STRING DeviceName{ 0 };

	RtlInitUnicodeString( &DeviceName, WANDER_DEVICE_NAME );
	RtlInitUnicodeString( &WASymbolicLinkName, WANDER_DOS_DEVICE_NAME );
	
	auto StatusCode = IoCreateDevice( DriverObject_, 0, &DeviceName, WANDER_DEVICE_TYPE, 0, FALSE, &WADeviceObject );

	if ( NT_SUCCESS( StatusCode ) )
	{
		DriverObject_->MajorFunction[ IRP_MJ_CREATE ] = WADispatchCreate;
		DriverObject_->MajorFunction[ IRP_MJ_CLOSE ] = WADispatchClose;
		DriverObject_->MajorFunction[ IRP_MJ_DEVICE_CONTROL ] = WADispatchDeviceControl;

		DriverObject_->DriverUnload = WAUnload;

		StatusCode = IoCreateSymbolicLink( &WASymbolicLinkName, &DeviceName );

		if ( NT_SUCCESS( StatusCode ) )
		{
			WADeviceObject->Flags &= ~DO_DEVICE_INITIALIZING;
			WADeviceObject->Flags |= DO_DIRECT_IO;

			DPRINT( "DriverEntry: Driver loaded \n" );
			return STATUS_SUCCESS;
		}

		DPRINT( "DriverEntry: IoCreateSymbolicLink failed with status code %lX \n", StatusCode );
		return StatusCode;
	}

	DPRINT( "DriverEntry: IoCreateDevice failed with status code: %lX \n", StatusCode );
	return StatusCode;
}