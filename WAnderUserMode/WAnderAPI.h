#pragma once
#define WINVER 0x0A00 
#define _WIN32_WINNT 0x0A00  

#include <Winternl.h>
#include <ntifs.h>

class WA_PROCESSID_IN
{
public:
	explicit WA_PROCESSID_IN( wchar_t* ProcessName_ )
		: m_ProcessName( { 0 } )
	{
		RtlInitUnicodeString( &m_ProcessName, ProcessName_ );
	}

	explicit WA_PROCESSID_IN( char* ProcessName_ )
		: m_ProcessName( { 0 } )
	{
		RtlInitUnicodeString( &m_ProcessName, RtlAnsiCharToUnicodeChar(  ) );
	}

	UNICODE_STRING m_ProcessName;
};

class WA_PROCESSID_OUT
{
	
};