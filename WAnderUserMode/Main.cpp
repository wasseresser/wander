#include <iostream>
#include <Windows.h>

typedef struct _UNICODE_STRING
{
	USHORT Length;
	USHORT MaximumLength;
#ifdef MIDL_PASS
	[ size_is( MaximumLength / 2 ), length_is( ( Length ) / 2 ) ] USHORT * Buffer;
#else // MIDL_PASS
	_Field_size_bytes_part_opt_( MaximumLength, Length ) PWCH   Buffer;
#endif // MIDL_PASS
} UNICODE_STRING;

typedef LONG KPRIORITY;

typedef struct _SYSTEM_PROCESS_INFORMATION
{
	ULONG NextEntryOffset;
	ULONG NumberOfThreads;
	LARGE_INTEGER SpareLi1;
	LARGE_INTEGER SpareLi2;
	LARGE_INTEGER SpareLi3;
	LARGE_INTEGER CreateTime;
	LARGE_INTEGER UserTime;
	LARGE_INTEGER KernelTime;
	UNICODE_STRING ImageName;
	KPRIORITY BasePriority;
	HANDLE UniqueProcessId;
	HANDLE InheritedFromUniqueProcessId;
	ULONG HandleCount;
	ULONG SessionId;
	ULONG_PTR PageDirectoryBase;
	SIZE_T PeakVirtualSize;
	SIZE_T VirtualSize;
	ULONG PageFaultCount;
	SIZE_T PeakWorkingSetSize;
	SIZE_T WorkingSetSize;
	SIZE_T QuotaPeakPagedPoolUsage;
	SIZE_T QuotaPagedPoolUsage;
	SIZE_T QuotaPeakNonPagedPoolUsage;
	SIZE_T QuotaNonPagedPoolUsage;
	SIZE_T PagefileUsage;
	SIZE_T PeakPagefileUsage;
	SIZE_T PrivatePageCount;
	LARGE_INTEGER ReadOperationCount;
	LARGE_INTEGER WriteOperationCount;
	LARGE_INTEGER OtherOperationCount;
	LARGE_INTEGER ReadTransferCount;
	LARGE_INTEGER WriteTransferCount;
	LARGE_INTEGER OtherTransferCount;
} SYSTEM_PROCESS_INFORMATION, *PSYSTEM_PROCESS_INFORMATION;

NTSTATUS WAGetProcessId( wchar_t*, SYSTEM_PROCESS_INFORMATION* );

#define WANDER_DEVICE_TYPE 0x1908

#define WANDER_IOCTL( I ) CTL_CODE( WANDER_DEVICE_TYPE, 0x800 + I, METHOD_NEITHER, FILE_ANY_ACCESS )

#define IOCTL_WANDER_GET_PROCESS_ID WANDER_IOCTL( 1 )

int main( uint32_t Argc_, uint8_t* Argv_[ ] )
{
	auto Driver = CreateFileW( L"\\\\.\\WAnder", GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, 0, nullptr );

	if ( Driver == INVALID_HANDLE_VALUE )
		std::cout << "Invalid Handle: " << GetLastError( ) << '\n';

	SYSTEM_PROCESS_INFORMATION ProcessInfoBuffer{ 0 };
	wchar_t* ProcessName{ L"explorer.exe" };
	
	if ( !DeviceIoControl( Driver, IOCTL_WANDER_GET_PROCESS_ID, ProcessName, sizeof( ProcessName ), &ProcessInfoBuffer, sizeof( ProcessInfoBuffer ), nullptr, nullptr ) )
		std::cout << "DeviceIoControl failed with: " << GetLastError( ) << '\n';

	std::wcout << "ImageName: " << ProcessInfoBuffer.ImageName.Buffer << '\n';
	std::cout << "NumberOfThreads: " << ProcessInfoBuffer.NumberOfThreads << '\n';
	std::cout << "UniqueProcessId: " << ProcessInfoBuffer.UniqueProcessId << '\n';

	std::cin.get( );

	return EXIT_SUCCESS;
}
